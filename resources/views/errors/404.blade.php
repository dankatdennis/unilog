@extends('layouts.main')

@section('content')

    <div class="container">

        <div class="row">

            <div class="page-not-found">
    
                <img src="/img/pagenotfound.png" class="media-object">

        </div>

        </div>
       



  
            <div class="row">

                <div class="error">
    
                        <h2>Page Not Found</h2>
    
                        <p>Sorry The Page You are Looking For is Not Available</p>

                        <button  class="reload" href="{{ route('blog')}}">GO BACK HOME</button>
    
                </div>
    
            </div>
    
    
        </div>
    
@endsection