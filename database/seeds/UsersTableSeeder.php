<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        //reset the users table
        DB::table('users')->truncate();


        //generate 3 users

        $faker = Factory::create();

        Db::table('users')->insert([
            [
                'name' => "John Doe",
                'slug' => "john-doe",
                'email' => "Johndoe@gmail.com",
                'password' => bcrypt('secret'),
                'bio' => $faker->text(rand(100, 200))

            ],
            [
                'name' => "Queen Tette",
                'slug' => "jane-doe",
                'email' => "tette@gmail.com",
                'password'=> bcrypt('secret'),
                'bio' => $faker->text(rand(100, 200))
            ],
            [
                'name' => "Daudi Kabaka",
                'slug' => "daudi-kabaka",
                'email' => "dkabaka@gmail.com",
                'password'=> bcrypt('secret'),
                'bio' => $faker->text(rand(100, 200))
            ]
        ]);
    }
}
